<?php declare(strict_types=1);

use GDXbsv\PServiceBus\Transport\InMemoryTransport;
use GDXbsv\PServiceBusTestApp\HandlingExternal\InMemoryExternalTransport;

return [
    'transports' => [
        'external' => InMemoryExternalTransport::class,
        'main' => InMemoryTransport::class,
    ],
    'log_not_throw_errors' => false,
    'psr4_vendors_enabled' => true,
];
