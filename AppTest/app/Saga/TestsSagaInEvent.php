<?php declare(strict_types=1);

namespace App\Saga;

final class TestsSagaInEvent
{
    public ?string $string = 'testSaga';
    public ?string $value = 'secretValue';
}
