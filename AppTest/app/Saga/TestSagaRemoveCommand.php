<?php declare(strict_types=1);

namespace App\Saga;

class TestSagaRemoveCommand
{
    public string $id = 'testSaga';
    public string $string = 'testSagaString';
}
