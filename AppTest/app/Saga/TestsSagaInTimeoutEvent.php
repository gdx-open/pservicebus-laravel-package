<?php declare(strict_types=1);

namespace App\Saga;

final class TestsSagaInTimeoutEvent
{
    public ?string $string = 'testSaga';
    public ?string $value = 'secretValue';
}
