<?php declare(strict_types=1);

namespace App\Saga;

final class CustomSearchEvent
{
    public string $string = 'testSaga';
    public string $value = 'secretValue';
}
