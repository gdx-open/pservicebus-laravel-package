<?php declare(strict_types=1);

namespace App\Saga;

class TestSagaMapStringCommand
{
    public string $string = 'testSaga';
}
