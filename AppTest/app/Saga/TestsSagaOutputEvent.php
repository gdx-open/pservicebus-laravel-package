<?php declare(strict_types=1);

namespace App\Saga;

final class TestsSagaOutputEvent
{
    public string $result;

    public function __construct(string $result)
    {
        $this->result = $result;
    }
}
