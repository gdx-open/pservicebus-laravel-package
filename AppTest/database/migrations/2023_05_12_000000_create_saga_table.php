<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saga', function (Blueprint $table) {
            $table->integer('playhead')->nullable(false);
            $table->string('id')->unique()->primary()->nullable(false);
            $table->string('string')->nullable(false);
            $table->string('value')->nullable(true);
            $table->timestamp('completedAt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saga');
    }
};
