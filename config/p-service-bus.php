<?php declare(strict_types=1);

return [
    /**
     * Difine your transports here
     *     'transports' => [
     *       'external' => InMemoryExternalTransport::class,
     *       'main' => InMemoryTransport::class,
     *      ],
     */
    'transports' => [
    ],

    /**
     * Define namespace to search attributes
     */
    'namespace_for_search' => 'App\\',
    /**
     *  true => log and not exit if something goes wrong
     *  false => throw error and exit if something goes wrong
     */
    'log_not_throw_errors' => true,
    /**
     * false => load only user defined namespaces
     * true => load every namespace (can be useful for tests)
     */
    'psr4_vendors_enabled' => false,
    /**
     * OnlyOnce configuration
     */
    'only_once' => [
        /**
         * table name
         */
        'table' => 'service_bus_only_once'
    ],
    /**
     * Outbox configuration
     */
    'outbox' => [
        /**
         * table name
         */
        'table' => 'service_bus_outbox'
    ],
];
