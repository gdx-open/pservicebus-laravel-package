<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('p-service-bus.only_once.table'), function (Blueprint $table) {
            $table->primary(['message_id', 'handler']);
            $table->string('message_id', 36)->nullable(false);
            $table->string('handler')->nullable(false);
            $table->timestamp('inserted_at')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('p-service-bus.only_once.table'));
    }
};
