<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('p-service-bus.outbox.table'), function (Blueprint $table) {
            $table->string('message_id', 36)->unique()->primary()->nullable(false);
            $table->text('message')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('p-service-bus.outbox.table'));
    }
};
