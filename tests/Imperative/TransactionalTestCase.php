<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusPackageTests\Imperative;

use Illuminate\Database\DatabaseManager;
use Illuminate\Foundation\Http\Kernel;
use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Support\Facades\DB;
use JetBrains\PhpStorm\Immutable;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[Immutable]
abstract class TransactionalTestCase extends TestCase
{
    public $mockConsoleOutput = false;
    public function createApplication()
    {
        $app = require __DIR__ . '/../../AppTest/bootstrap/app.php';
        $app->make(Kernel::class)->bootstrap();
        return $app;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->app->get(DatabaseManager::class)->beginTransaction();
    }

    protected function tearDown(): void
    {
        $this->app->get(DatabaseManager::class)->rollBack();
        parent::tearDown();
    }
}
