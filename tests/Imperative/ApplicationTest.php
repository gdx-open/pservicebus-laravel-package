<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusPackageTests\Imperative;

use GDXbsv\PServiceBus\Bus;
use Illuminate\Foundation\Http\Kernel;
use Illuminate\Foundation\Testing\TestCase;

/**
 * @internal
 */
final class ApplicationTest extends TestCase
{
    public function createApplication()
    {
        $app = require __DIR__ . '/../../AppTest/bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    public function test_boot_application()
    {
        $app = $this->app;

        $this->assertInstanceOf(
            Bus::class,
            $app->get(Bus\ServiceBus::class)
        );
    }
}
