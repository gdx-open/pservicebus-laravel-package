<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusPackageTests\Imperative;

use App\Saga\TestSagaCreateCommand;
use App\Saga\TestSagaModel;
use App\Saga\TestSagaRemoveCommand;
use App\Saga\TestsSagaOutputEvent;
use GDXbsv\PServiceBus\Bus;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Model;

/**
 * @internal
 */
final class SagaTest extends TransactionalTestCase
{

    public function testCreating(): void
    {
        $app = $this->app;
        /** @var DatabaseManager $dm */
        $dm = $app->get(DatabaseManager::class);
        $bus = $app->get(Bus::class);
        $bus->trace();
        $bus->send($commandCreate = new TestSagaCreateCommand());
        /** @var ?Model[] $saga */
        $sagaModels = $dm->table((new TestSagaModel())->getTable())->where((new TestSagaModel())->getKeyName(), $commandCreate->id)->get();
        self::assertCount(1, $sagaModels);
        $sagaModel = $sagaModels[0];
        self::assertEquals($commandCreate->string, $sagaModel->string);
    }

    public function testFind(): void
    {
        $app = $this->app;
        /** @var DatabaseManager $dm */
        $dm = $app->get(DatabaseManager::class);
        $bus = $app->get(Bus::class);
        $bus->trace();
        $bus->send($commandCreate = new TestSagaCreateCommand());
        /** @var ?Model[] $saga */
        $sagaModels = $dm->table((new TestSagaModel())->getTable())->where((new TestSagaModel())->getKeyName(), $commandCreate->id)->get();
        self::assertCount(1, $sagaModels);
        $sagaModel = $sagaModels[0];
        self::assertEquals($commandCreate->string, $sagaModel->string);

        $commandCreateSecond = new TestSagaCreateCommand();
        $commandCreateSecond->string = 'second time';
        $bus->send($commandCreateSecond);

        /** @var ?Model[] $saga */
        $sagaModels = $dm->table((new TestSagaModel())->getTable())->where((new TestSagaModel())->getKeyName(), $commandCreate->id)->get();
        self::assertCount(1, $sagaModels);
        $sagaModel = $sagaModels[0];
        self::assertEquals($commandCreateSecond->string, $sagaModel->string);
    }

    public function testRemoving(): void
    {
        $app = $this->app;
        /** @var Bus\TraceableBus $bus */
        $bus = $app->get(Bus::class);
        /** @var DatabaseManager $dm */
        $dm = $app->get(DatabaseManager::class);

        $bus->trace();
        $bus->send($commandCreate = new TestSagaCreateCommand());
        /** @var ?Model[] $saga */
        $sagaModels = $dm->table((new TestSagaModel())->getTable())->where((new TestSagaModel())->getKeyName(), $commandCreate->id)->get();
        self::assertCount(1, $sagaModels);

        $bus->send($commandRemove = new TestSagaRemoveCommand());

        /** @var ?Model[] $saga */
        $sagaModels = $dm->table((new TestSagaModel())->getTable())->where((new TestSagaModel())->getKeyName(), $commandCreate->id)->get();
        self::assertCount(0, $sagaModels);
    }

    public function testOutboxRecovery(): void
    {
        $app = $this->app;
        /** @var Bus\TraceableBus $bus */
        $bus = $app->get(Bus::class);
        /** @var DatabaseManager $dm */
        $dm = $app->get(DatabaseManager::class);

        $dm->table(config('p-service-bus.outbox.table'))
            ->insert([
                'message_id' => '01890234-6a55-736a-820e-64964ed8a21b',
                'message' => 'O:34:"GDXbsv\PServiceBus\Message\Message":2:{s:7:"payload";O:29:"App\Saga\TestsSagaOutputEvent":1:{s:6:"result";s:19:"testHandlerFunction";}s:7:"options";O:39:"GDXbsv\PServiceBus\Message\EventOptions":5:{s:7:"timeout";O:35:"GDXbsv\PServiceBus\Message\TimeSpan":1:{s:11:"intervalSec";i:0;}s:7:"retries";i:3;s:9:"messageId";O:21:"GDXbsv\PServiceBus\Id":1:{s:28:" GDXbsv\PServiceBus\Id value";s:36:"0189061e-4674-7013-99f8-4c3c79d70251";}s:10:"recordedOn";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2023-06-29 07:46:34.740880";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:7:"headers";a:2:{s:12:"message_type";s:5:"event";s:8:"playhead";i:0;}}}'
            ]);

        $bus->trace();
        $return = $this->artisan('p-service-bus:saga:eloquent:outbox:recover-messages');
        self::assertEquals(0, $return);

        $message = $bus->getEventMessageByClass(TestsSagaOutputEvent::class);
        self::assertEquals('testHandlerFunction', $message->payload->result);
    }

    public function testClean(): void
    {
        $app = $this->app;
        /** @var DatabaseManager $dm */
        $dm = $app->get(DatabaseManager::class);

        $dm->table(config('p-service-bus.only_once.table'))
            ->insert([
                'message_id' => '01890234-6a55-736a-820e-64964ed8a21b',
                'handler' => 'testName',
                'inserted_at' => (new \DateTimeImmutable("-31 days"))->format('Y-m-d H:i:s'),
            ]);
        $dm->table(config('p-service-bus.only_once.table'))
            ->insert([
                'message_id' => '01890234-6a55-736a-820e-64964ed8a21b',
                'handler' => 'testName2',
                'inserted_at' => (new \DateTimeImmutable("-29 days"))->format('Y-m-d H:i:s'),
            ]);

        $return = $this->artisan('p-service-bus:saga:eloquent:only-once:clean');
        self::assertEquals(0, $return);

        $records = $dm->table(config('p-service-bus.only_once.table'))->get();
        self::assertCount(1, $records);
    }
}
