<?php declare(strict_types=1);

require __DIR__ . '/../../vendor/autoload.php';

use Illuminate\Foundation\Http\Kernel;

function createApplication()
{
    $app = require __DIR__ . '/../../AppTest/bootstrap/app.php';
    $app->make(Kernel::class)->bootstrap();
    return $app;
}

$app = createApplication();
return $app[\Illuminate\Contracts\Console\Kernel::class]
    ->call(
        'migrate',
        ['--database' => 'mysql', '--force' => true, '--path' => ['database/migrations', '../database/migrations']]
    );
