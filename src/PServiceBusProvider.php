<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusLaravel;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\ConsumeBus;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\NotControlling;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceControl;
use GDXbsv\PServiceBus\Bus\Handling\ScrapeHandlers;
use GDXbsv\PServiceBus\Bus\ServiceBus;
use GDXbsv\PServiceBus\Message\ExternalIn;
use GDXbsv\PServiceBus\Message\ExternalOut;
use GDXbsv\PServiceBus\Message\ScrapeExternals;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaHandling;
use GDXbsv\PServiceBus\Saga\SagaMapper;
use GDXbsv\PServiceBus\Saga\SagaPersistence;
use GDXbsv\PServiceBus\Saga\ScrapeFinders;
use GDXbsv\PServiceBus\Serializer\ClojureSerializer;
use GDXbsv\PServiceBus\Serializer\Serializer;
use GDXbsv\PServiceBus\Transport\ConsumeConsoleCommand;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;
use GDXbsv\PServiceBus\Transport\TransportSyncConsoleCommand;
use GDXbsv\PServiceBus\Transport\TransportSynchronisation;
use GDXbsv\PServiceBusLaravel\Bus\BusErrorHandler;
use GDXbsv\PServiceBusLaravel\Saga\EloquentOnlyOnceControl;
use GDXbsv\PServiceBusLaravel\Saga\OnlyOnceCleanConsoleCommand;
use GDXbsv\PServiceBusLaravel\Saga\OutboxRecoverConsoleCommand;
use GDXbsv\PServiceBusLaravel\Saga\SagaEloquent;
use GDXbsv\PServiceBusLaravel\Saga\SagaEloquentPersistence;
use HaydenPierce\ClassFinder\ClassFinder;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\DatabaseManager;
use Illuminate\Foundation\Console\ClosureCommand;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use ProxyManager\Proxy\LazyLoadingInterface;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\StringInput;

class PServiceBusProvider extends ServiceProvider
{
    private bool $inited = false;
    private string $cacheDirectory;
    private \ProxyManager\Factory\LazyLoadingValueHolderFactory $proxyFactory;

    private function init(): void
    {
        if ($this->inited) {
            return;
        }
        $this->inited = true;
        $cacheDirectory = App::storagePath() . DIRECTORY_SEPARATOR . 'framework' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'p-service-bus';
        if (!is_dir($cacheDirectory)) {
            if (!mkdir($cacheDirectory, 0775, true) && !is_dir($cacheDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $cacheDirectory));
            }
        }
        $this->cacheDirectory = $cacheDirectory;


        $proxyCacheDir = $cacheDirectory . '/proxy';
        if (!is_dir($proxyCacheDir)) {
            if (!mkdir($proxyCacheDir, 0775, true) && !is_dir($proxyCacheDir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $proxyCacheDir));
            }
        }
        $config = new \ProxyManager\Configuration();
        // generate the proxies and store them as files
        $fileLocator = new \ProxyManager\FileLocator\FileLocator($proxyCacheDir);
        $config->setGeneratorStrategy(new \ProxyManager\GeneratorStrategy\FileWriterGeneratorStrategy($fileLocator));
        // set the directory to read the generated proxies from
        $config->setProxiesTargetDir($proxyCacheDir);
        // then register the autoloader
        spl_autoload_register($config->getProxyAutoloader());
        // pass the configuration to proxymanager factory
        $factory = new \ProxyManager\Factory\LazyLoadingValueHolderFactory($config);
        $this->proxyFactory = $factory;
    }

    public function boot(): void
    {
        $environment = App::environment();
        $this->publishes([
            __DIR__ . '/../config/p-service-bus.php' => config_path('p-service-bus.php'),
        ], 'p-service-bus-config');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');


        $this->init();
        $transportMap = [];
        $transportsConfig = config('p-service-bus.transports');
        foreach ($transportsConfig as $name => $serviceName) {
            $transportMap[$name] = $this->app->make($serviceName);
        }
        $this->app->extend(ServiceBus::class, function (ServiceBus $service, Application $app) use ($transportMap) {
            $buildStorage = $app->make('p-service-bus.build.storage');
            $bus = new ServiceBus(
                $app->make(SagaHandling::class),
                $app->make(Serializer::class),
                $transportMap,
                $buildStorage->handlingInstructions,
                $buildStorage->handlerToObjectMap,
                $buildStorage->messageInClassMap,
                $buildStorage->messageOutClassMap,
                $buildStorage->messageNameMap
            );

            if (config('p-service-bus.log_not_throw_errors')) {
                $bus->setErrorHandler(\Closure::fromCallable($app->make(BusErrorHandler::class)));
            }

            return $bus;
        });

        if ($environment === 'testing') {
            $this->app->singleton(Bus\TraceableBus::class, fn(Application $app) => new Bus\TraceableBus($app->make(ServiceBus::class)));
            $this->app->bind(Bus::class, Bus\TraceableBus::class);
            $this->app->bind(Bus\CoroutineBus::class, Bus\TraceableBus::class);
            $this->app->bind(ConsumeBus::class, Bus\TraceableBus::class);
        }

        if ($this->app->runningInConsole()) {
            $this->app->extend(TransportSyncConsoleCommand::class, function (TransportSyncConsoleCommand $service, Application $app) use ($transportMap) {
                $transportsSync = [];
                foreach ($transportMap as $transport) {
                    if (is_subclass_of($transport, TransportSynchronisation::class)) {
                        $transportsSync[] = $transport;
                    }
                }
                return new TransportSyncConsoleCommand($transportsSync);
            });
            $this->app->extend(ConsumeConsoleCommand::class, function (ConsumeConsoleCommand $service, Application $app) use ($transportMap) {
                return new ConsumeConsoleCommand($app->make(ConsumeBus::class), $transportMap);
            });
            $this->commands([
                OutboxRecoverConsoleCommand::class,
                OnlyOnceCleanConsoleCommand::class,
            ]);
        }
    }

    public function register(): void
    {
        $this->init();
        $environment = App::environment();
        $rootCatalog = App::basePath();

        $this->mergeConfigFrom(
            __DIR__ . '/../config/p-service-bus.php', 'p-service-bus'
        );

        if (!App::hasDebugModeEnabled()
            && file_exists($this->cacheDirectory . '/messageClassMapOut')
            && file_exists($this->cacheDirectory . '/messageNameMapOut')
            && file_exists($this->cacheDirectory . '/messageClassMapIn')
            && file_exists($this->cacheDirectory . '/messageNameMapIn')
            && file_exists($this->cacheDirectory . '/handlingInstructions')
            && file_exists($this->cacheDirectory . '/sagaFindInstructions')
        ) {
            eval('$messageClassMapOut = ' . file_get_contents($this->cacheDirectory . '/messageClassMapOut') . ';');
            eval('$messageNameMapOut = ' . file_get_contents($this->cacheDirectory . '/messageNameMapOut') . ';');
            eval('$messageClassMapIn = ' . file_get_contents($this->cacheDirectory . '/messageClassMapIn') . ';');
            eval('$messageNameMapIn = ' . file_get_contents($this->cacheDirectory . '/messageNameMapIn') . ';');
            eval('$handlingInstructions = ' . file_get_contents($this->cacheDirectory . '/handlingInstructions') . ';');
            eval('$sagaFindInstructions = ' . file_get_contents($this->cacheDirectory . '/sagaFindInstructions') . ';');
            eval('$sagas = ' . file_get_contents($this->cacheDirectory . '/sagas') . ';');
        } else {

            if (!config('p-service-bus.psr4_vendors_enabled')) {
                ClassFinder::disablePSR4Vendors();
            }
            $classes = ClassFinder::getClassesInNamespace(config('p-service-bus.namespace_for_search'), ClassFinder::RECURSIVE_MODE);


            $handlingInstructions = ScrapeHandlers::fromClasses($classes);
            list($messageClassMapOut, $messageNameMapOut) = ScrapeExternals::fromClasses($classes, ExternalOut::class);
            list($messageClassMapIn, $messageNameMapIn) = ScrapeExternals::fromClasses($classes, ExternalIn::class);

            $sagaFindInstructions = ScrapeFinders::fromClasses($classes);
            /** @var list<class-string<Saga>> $sagas */
            $sagas = array_filter(
                $classes,
                fn(string $className) => is_subclass_of($className, Saga::class)
            );

            file_put_contents($this->cacheDirectory . '/messageClassMapOut', var_export($messageClassMapOut, true));
            file_put_contents($this->cacheDirectory . '/messageNameMapOut', var_export($messageNameMapOut, true));
            file_put_contents($this->cacheDirectory . '/messageClassMapIn', var_export($messageClassMapIn, true));
            file_put_contents($this->cacheDirectory . '/messageNameMapIn', var_export($messageNameMapIn, true));
            file_put_contents($this->cacheDirectory . '/handlingInstructions', var_export($handlingInstructions, true));
            file_put_contents($this->cacheDirectory . '/sagaFindInstructions', var_export($sagaFindInstructions, true));
            file_put_contents($this->cacheDirectory . '/sagas', var_export($sagas, true));
        }


#####>TRANSPORTS
        $transportMap = [];
        $this->app->singleton(InMemoryTransport::class, fn(Application $app) => new InMemoryTransport());
        $this->app->singleton(InMemoryExternalTransport::class, fn(Application $app) => new InMemoryExternalTransport());
        $transportsConfig = config('p-service-bus.transports');
        foreach ($transportsConfig as $name => $serviceName) {
            $transportMap[$name] = $this->app->make(InMemoryTransport::class);
        }
#####<TRANSPORTS


        $this->app->singleton(ClojureSerializer::class, fn(Application $app) => new ClojureSerializer());
        $this->app->bind(Serializer::class, ClojureSerializer::class);

        $messageInClassMap = $messageClassMapIn;
        $messageOutClassMap = $messageClassMapOut;
        $messageNameMap = array_merge($messageNameMapOut, $messageNameMapIn);

        $handlerToObjectMap = [];
        foreach ($handlingInstructions as $handlingInstructionsMessage) {
            foreach ($handlingInstructionsMessage as $handlingInstruction) {
                $className = $handlingInstruction->handlerName;
                if (!isset($handlerToObjectMap[$className]) && !is_subclass_of($className, Saga::class)) {
                    $initializer = function (&$wrappedObject, LazyLoadingInterface $proxy, $method, array $parameters, &$initializer) use ($className) {
                        $initializer = null; // disable initialization
                        $wrappedObject = $this->app->make($className); // fill your object with values here

                        return true; // confirm that initialization occurred correctly
                    };

                    $handlerToObjectMap[$className] = $this->proxyFactory->createProxy($className, $initializer);
                }
            }
        }
        $sagaFindToObjectMap = [];
        foreach ($sagaFindInstructions as $sagaInstructionsMessage) {
            foreach ($sagaInstructionsMessage as $sagaInstruction) {
                $className = $sagaInstruction->className;
                if (!isset($sagaFindToObjectMap[$className]) && !is_subclass_of($className, SagaEloquent::class)) {
                    $initializer = function (&$wrappedObject, LazyLoadingInterface $proxy, $method, array $parameters, &$initializer) use ($className) {
                        $initializer = null; // disable initialization
                        $wrappedObject = $this->app->make($className); // fill your object with values here

                        return true; // confirm that initialization occurred correctly
                    };

                    $sagaFindToObjectMap[$className] = $this->proxyFactory->createProxy($className, $initializer);
                }
            }
        }

        $this->app->singleton(EloquentOnlyOnceControl::class, fn(Application $app) => new EloquentOnlyOnceControl(
            $app->make(DatabaseManager::class),
            config('p-service-bus.only_once.table'),
        ));
        $this->app->bind(OnlyOnceControl::class, EloquentOnlyOnceControl::class);
        $this->app->singleton(SagaMapper::class, fn(Application $app) => new SagaMapper($sagas));
        $this->app->singleton(SagaEloquentPersistence::class,
            fn(Application $app) => new SagaEloquentPersistence(
                $app->make(DatabaseManager::class),
                $app->make(OnlyOnceControl::class),
                $app->make(SagaMapper::class),
                config('p-service-bus.outbox.table'),
                $sagaFindInstructions,
                $sagaFindToObjectMap,
            )
        );
        $this->app->bind(SagaPersistence::class, SagaEloquentPersistence::class);
        $this->app->singleton(SagaHandling::class, fn(Application $app) => new SagaHandling($app->make(SagaPersistence::class)));


        $this->app->singleton(
            ServiceBus::class,
            fn(Application $app) => new ServiceBus(
                $app->make(SagaHandling::class),
                $app->make(Serializer::class),
                $transportMap,
                $handlingInstructions,
                $handlerToObjectMap,
                $messageInClassMap,
                $messageOutClassMap,
                $messageNameMap,
            )
        );
        $this->app->singleton(
            'p-service-bus.build.storage',
            fn(Application $app) => new class(
                $handlingInstructions,
                $handlerToObjectMap,
                $messageInClassMap,
                $messageOutClassMap,
                $messageNameMap,
                $messageNameMapIn,
            ) {
                public function __construct(
                    public array $handlingInstructions,
                    public array $handlerToObjectMap,
                    public array $messageInClassMap,
                    public array $messageOutClassMap,
                    public array $messageNameMap,
                    public array $messageNameMapIn,
                )
                {
                }
            }
        );
        $this->app->bind(Bus::class, ServiceBus::class);
        $this->app->bind(Bus\CoroutineBus::class, ServiceBus::class);
        $this->app->bind(ConsumeBus::class, ServiceBus::class);

        $this->app->extend(InMemoryTransport::class, function (InMemoryTransport $service, Application $app) {
            $service->setBus($app->make(Bus::class));
            return $service;
        });
        $this->app->extend(SagaPersistence::class, function (SagaPersistence $service, Application $app) {
            $service->setCoroutineBus($app->make(Bus\CoroutineBus::class));
            return $service;
        });


        if ($this->app->runningInConsole()) {
            $cacheFolder = $this->cacheDirectory;
            Artisan::command(
                'p-service-bus:cache:clear',
                function () use ($cacheFolder): int {
                    File::deleteDirectory($cacheFolder);
                    /** @var ClosureCommand $self */
                    $self = $this;
                    $self->info('Cleared');
                    return 0;
                }
            );
            $this->app->singleton(TransportSyncConsoleCommand::class, fn(Application $app) => new TransportSyncConsoleCommand([]));
            Artisan::command(
                'p-service-bus:transport:sync',
                function (TransportSyncConsoleCommand $consoleCommand) {
                    /** @var ClosureCommand $self */
                    $self = $this;
                    $output = $self->getOutput()->getOutput();
                    return $consoleCommand->run(new ArgvInput([]), $output);
                }
            );
            $this->app->singleton(ConsumeConsoleCommand::class, fn(Application $app) => new ConsumeConsoleCommand($app->make(ConsumeBus::class), []));
            Artisan::command(
                'p-service-bus:transport:consume {transport?} {--limit=0}',
                function (ConsumeConsoleCommand $consoleCommand, ?string $transport = null, int $limit = 0) {
                    /** @var ClosureCommand $self */
                    $self = $this;
                    $output = $self->getOutput()->getOutput();
                    return $consoleCommand->run(new StringInput("$transport --limit=$limit"), $output);
                }
            );
        }
    }
}
