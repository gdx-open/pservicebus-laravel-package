<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusLaravel\Bus;

use GDXbsv\PServiceBus\Message\Message;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

final class BusErrorHandler
{
    public function __construct(public LoggerInterface $logger)
    {
    }

    public function __invoke(\Throwable $t, Message $message): void
    {
        if (class_exists('\Doctrine\DBAL\Exception', false) && $t instanceof \Doctrine\DBAL\Exception) {
            throw $t;
        }
        if (class_exists('\PDOException', false) && $t instanceof \PDOException) {
            throw $t;
        }

        $level = LogLevel::ERROR;
        $from = $message->options->headers['message_type'] ?? 'TYPE NOT DEFINED LOOKS LIKE AN ERROR';
        $type = $message->payload::class;
        $logMessage = "Consume for '{$from}: {$type}' failed.";


        $this->logger->log(
            $level,
            $logMessage,
            [
                'from' => $from,
                'type' => $type,
                'messageInternalId' => $message->options->messageId->toString(),
                'recordedOn' => $message->options->recordedOn->format('Y-m-d\TH:i:s.uP'),
                'headers' => $message->options->headers,
                'exception' => $t,
            ]
        );
    }
}
