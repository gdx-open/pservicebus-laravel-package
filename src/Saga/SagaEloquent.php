<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusLaravel\Saga;

use Doctrine\Instantiator\Instantiator;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Saga\Saga;
use Illuminate\Database\Eloquent\Model;

abstract class SagaEloquent extends Saga
{
    protected Model $eloquentModel;

    public static function fromEloquentModel(Model $model): static
    {
        $config = new \GeneratedHydrator\Configuration(static::class);
        $hydratorClass = $config->createFactory()->getHydratorClass();
        $hydrator = new $hydratorClass();
        $object = (new Instantiator())->instantiate(static::class);

        $values = $model->toArray();
        $values['_playhead'] = $values['playhead'];
        unset($values['playhead'],);

        $classReflection = new \ReflectionClass($object);
        foreach ($classReflection->getProperties() as $property) {
            if (!$property->hasType()) {
                continue;
            }
            /** @var \ReflectionNamedType $propertyType */
            $propertyType = $property->getType();
            $class = $propertyType->getName();
            if ($class === Id::class) {
                $values[$property->getName()] = new Id($values[$property->getName()]);
                break;
            }
        }

        return $hydrator->hydrate(
            [... $values, 'eloquentModel' => $model],
            $object,
        );
    }

    /**
     * @return class-string<Model>
     */
    abstract public static function getEloquentModelClass(): string;

    private function toEloquentModel(): Model
    {
        $config = new \GeneratedHydrator\Configuration(static::class);
        $hydratorClass = $config->createFactory()->getHydratorClass();
        $hydrator = new $hydratorClass();
        $values = $hydrator->extract($this);
        $values['playhead'] = $values['_playhead'];
        unset($values['eloquentModel'], $values['_playhead']);
        $this->eloquentModel->fill($values);
        return $this->eloquentModel;
    }
}
