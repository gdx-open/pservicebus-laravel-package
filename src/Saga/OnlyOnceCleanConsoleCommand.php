<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusLaravel\Saga;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use JetBrains\PhpStorm\Immutable;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[Immutable]
final class OnlyOnceCleanConsoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'p-service-bus:saga:eloquent:only-once:clean {days=30 : days to keep}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send messages which left over in outbox';

    /**
     * Execute the console command.
     */
    public function handle(EloquentOnlyOnceControl $onlyOnceControl): void
    {
        $this->info('Start');
        $days = (int) $this->argument('days');
        $onlyOnceControl->cleanOld($days);
        $this->info('Finish');
    }
}
