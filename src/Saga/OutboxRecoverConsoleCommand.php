<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusLaravel\Saga;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use JetBrains\PhpStorm\Immutable;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[Immutable]
final class OutboxRecoverConsoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'p-service-bus:saga:eloquent:outbox:recover-messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send messages which left over in outbox';

    /**
     * Execute the console command.
     */
    public function handle(DatabaseManager $dm, Bus $bus): void
    {
        $this->info('Start');
        $bar = $this->output->createProgressBar();
        $bar->start();

        $dm->table(config('p-service-bus.outbox.table'))->orderBy('message_id')->each(function (object $record) use ($bar, $dm, $bus) {
            /** @var Message<EventOptions> $message */
            $message = unserialize($record->message);
            $bus->publish($message->payload, $message->options);
            $dm->table(config('p-service-bus.outbox.table'))
                ->where('message_id', $record->message_id)
                ->delete();

            $bar->advance();
        });
        $bar->finish();
        $this->info('Finish');
    }
}
