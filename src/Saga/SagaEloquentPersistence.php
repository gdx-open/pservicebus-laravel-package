<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusLaravel\Saga;

use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceControl;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Saga\MessageSagaContext;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaFindDefinitions;
use GDXbsv\PServiceBus\Saga\SagaFindInstruction as FindersMap;
use GDXbsv\PServiceBus\Saga\SagaPersistence;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * @internal
 * @psalm-import-type FindersMap from \GDXbsv\PServiceBus\Saga\ScrapeFinders
 */
final class SagaEloquentPersistence implements SagaPersistence
{
    private CoroutineBus $coroutineBus;

    /**
     * @param FindersMap $findInstructions
     * @param array<non-empty-string, object> $findToObjectMap
     */
    public function __construct(
        private DatabaseManager     $databaseManager,
        private OnlyOnceControl     $onlyOnceControl,
        private SagaFindDefinitions $sagaFinding,
        private string              $tableOutbox,
        private array               $findInstructions,
        private array               $findToObjectMap
    )
    {
    }

    public function retrieveSaga(Message $message, string $sagaType): ?Saga
    {
        $saga = null;
        $payload = $message->payload;
        $dm = $this->databaseManager;
        $dm->beginTransaction();
        if (!$this->onlyOnceControl->continue($message)) {
            $messageId = $message->options->messageId->toString();
            throw new \Exception("OnceControl: Message id already consumed: '{$messageId}'");
        }
        if (isset($this->findInstructions[$sagaType][$payload::class])) {
            $findInstruction = $this->findInstructions[$sagaType][$payload::class];
            assert(isset($this->findToObjectMap[$findInstruction->className]));
            /**
             * @var ?SagaEloquent $saga
             * @psalm-suppress MixedMethodCall
             */
            $saga = $this->findToObjectMap[$findInstruction->className]->{$findInstruction->methodName}(
                $payload,
                $message->options
            );
            if ($saga) {
                /** @var Model $model */
                $model = $this->extractModel($saga);
                $primaryKey = $model->getKeyName();
                /** @var Builder $builder */
                $builder = $model::lockForUpdate();
                $model = $builder->find($model->$primaryKey);
                $this->setModel($saga, $model);

                return $saga;
            }
        }

        foreach ($this->sagaFinding->sagasForMessage($payload) as $finderDefinition) {
            if ($finderDefinition->sagaType !== $sagaType) {
                continue;
            }
            /** @var mixed $propertyValue */
            $propertyValue = ($finderDefinition->messageProperty)(
                $payload,
                new MessageSagaContext($message->options)
            );

            /** @var class-string<SagaEloquent> $sagaClass */
            $sagaClass = $sagaType;
            /** @var Builder $builder */
            $builder = $sagaClass::getEloquentModelClass()::lockForUpdate();

            $models = $builder->where($finderDefinition->sagaProperty->getName(), '=', $propertyValue)->get();
            if (count($models) > 1) {
                throw new \RuntimeException("We found more than one models for {$sagaClass}. column: {$finderDefinition->sagaProperty->getName()}, value: {$propertyValue}");
            }
            if (count($models) === 1) {
                $saga = $sagaClass::fromEloquentModel($models[0]);
            }
        }

        if ($saga === null) {
            foreach ($this->sagaFinding->sagaCreatorsForMessage($payload) as $creatorDefinition) {
                if ($creatorDefinition->sagaType !== $sagaType) {
                    continue;
                }
                /** @var SagaEloquent $saga */
                $saga = ($creatorDefinition->sagaCreator)(
                    $payload,
                    new MessageSagaContext($message->options)
                );
                if ($saga) {
                    $this->setModel($saga, new ($saga::getEloquentModelClass())());
                }
            }
        }
        return $saga;
    }

    public function saveSaga(Saga $saga, array $messages): void
    {
        $dm = $this->databaseManager;
        /** @var SagaEloquent $sagaEloquent */
        $sagaEloquent = $saga;
        $model = $this->toModel($sagaEloquent);
        if ($saga->completedAt === null) {
            $model->save();
        } else {
            $model->delete();
        }
        $this->saveMessagesInTable($messages);
        $dm->commit();
        $coroutine = $this->coroutineBus->publishCoroutine();
        foreach ($messages as $message) {
            $coroutine->send($message);
        }
        $coroutine->send(null);
        $this->deleteMessagesFromTable($messages);
    }

    public function cleanSaga(Saga $saga): void
    {
        $dm = $this->databaseManager;
        try {
            $dm->rollBack();
        } catch (\Throwable $e) {
            //skip no transaction exception
        }
    }

    public function setCoroutineBus(CoroutineBus $coroutineBus): void
    {
        $this->coroutineBus = $coroutineBus;
    }

    /**
     * @param Message $messages
     */
    protected function saveMessagesInTable(array $messages): void
    {
        if (count($messages) === 0) {
            return;
        }
        $values = [];
        $parameters = [];
        foreach ($messages as $message) {
            $values[] = "(?, ?)";
            $parameters[] = $message->options->messageId->toString();
            $parameters[] = serialize($message);
        }
        $dm = $this->databaseManager;
        $dm->statement(
            sprintf(
                "INSERT INTO {$this->tableOutbox} (message_id, message) VALUES  %s",
                join(',', $values)
            ),
            $parameters
        );

    }

    /**
     * @param Message $messages
     */
    protected function deleteMessagesFromTable(array $messages): void
    {
        if (count($messages) === 0) {
            return;
        }
        $values = [];
        $parameters = [];
        foreach ($messages as $message) {
            $values[] = "?";
            $parameters[] = $message->options->messageId->toString();
        }
        $dm = $this->databaseManager;
        $dm->statement(
            sprintf(
                "DELETE FROM {$this->tableOutbox} WHERE message_id IN (%s)",
                join(',', $values)
            ),
            $parameters
        );
    }

    private function extractModel(SagaEloquent $sagaEloquent): Model
    {
        $closure = \Closure::bind(
            closure: static function (SagaEloquent $sagaEloquent): Model {
                /** @psalm-suppress InaccessibleProperty we can inside closure */
                return $sagaEloquent->eloquentModel;
            },
            newThis: null,
            newScope: SagaEloquent::class);

        /** * @psalm-suppress PossiblyNullFunctionCall * @psalm-suppress MixedReturnStatement */
        return $closure($sagaEloquent);
    }

    private function toModel(SagaEloquent $sagaEloquent): Model
    {
        $closure = \Closure::bind(
            closure: static function (SagaEloquent $sagaEloquent): Model {
                /** @psalm-suppress InaccessibleProperty we can inside closure */
                return $sagaEloquent->toEloquentModel();
            },
            newThis: null,
            newScope: SagaEloquent::class);

        /** * @psalm-suppress PossiblyNullFunctionCall * @psalm-suppress MixedReturnStatement */
        return $closure($sagaEloquent);
    }

    private function setModel(SagaEloquent $sagaEloquent, Model $model): void
    {
        $closure = \Closure::bind(
            closure: static function (SagaEloquent $sagaEloquent, Model $model): void {
                /** @psalm-suppress InaccessibleProperty we can inside closure */
                $sagaEloquent->eloquentModel = $model;
            },
            newThis: null,
            newScope: $sagaEloquent::class);

        /** * @psalm-suppress PossiblyNullFunctionCall * @psalm-suppress MixedReturnStatement */
        $closure($sagaEloquent, $model);
    }
}
