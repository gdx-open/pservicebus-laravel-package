<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusLaravel\Saga;

use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceControl;
use GDXbsv\PServiceBus\Message\Message;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\QueryException;

class EloquentOnlyOnceControl implements OnlyOnceControl
{
    public function __construct(private DatabaseManager $dm, private string $tableName)
    {
    }

    public function continue(Message $message): bool
    {
        if (!isset($message->options->headers['handlerName'], $message->options->headers['handlerMethodName'])) {
            return true;
        }
        $handler = (string)$message->options->headers['handlerName'] . '::' . (string)$message->options->headers['handlerMethodName'];
        $insertedAt = new \DateTimeImmutable();
        try {
            return $this->dm->table($this->tableName)
//                ->where('inserted_at', '<', $insertedAt)
                ->insert([
                    'message_id' => $message->options->messageId->toString(),
                    'handler' => $handler,
                    'inserted_at' => $insertedAt,
                ]);
        } catch (QueryException $e) {
            // unique error
            if ($e->getCode() === '23000') {
                return false;
            }

            throw $e;
        }
    }

    public function clean(Message $message): bool
    {
        if (!isset($message->options->headers['handlerName'], $message->options->headers['handlerMethodName'])) {
            return true;
        }
        $handler = (string)($message->options->headers['handlerName'] ?? '') . '::' . (string)($message->options->headers['handlerMethodName'] ?? '');

        $this->dm->table($this->tableName)
            ->where('message_id', '=', $message->options->messageId->toString())
            ->where('handler', '=', $handler)
            ->delete();

        return true;
    }

    public function cleanOld(int $days = 30): bool
    {
        $this->dm->table($this->tableName)
            ->where('inserted_at', '<', new \DateTimeImmutable("-$days days"))
            ->delete();

        return true;
    }
}
